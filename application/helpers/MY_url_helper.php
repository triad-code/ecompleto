<?php (defined('BASEPATH')) or exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package     CodeIgniter
 * @author      ExpressionEngine Dev Team
 * @copyright   Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license     http://codeigniter.com/user_guide/license.html
 * @link        http://codeigniter.com
 * @since       Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Url Helpers
 *
 * @package     CodeIgniter
 * @subpackage  Url
 * @category    Helpers
 */


if ( ! function_exists('get_youtube_id')){
    /**
     * [get_youtube_id Retorna o id de um video do youtube.]
     * @author Ralf da Rocha [ralf@ezoom.com.br]
     * @date   2014-07-01
     * @param  [string]     $value
     * @return [string]     $id
     */
    function get_youtube_id($value)
    {
        preg_match("/(?<=(v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\/)[^&\n]+|(?<=embed\/)[^\"&\n]+|(?<=(?:v|i)=)[^&\n]+|(?<=youtu.be\/)[^&\n]+/", $value, $matches);
        return isset($matches[0]) ? $matches[0] : false;
    }

}

if ( ! function_exists('get_youtube_img')){
    function get_youtube_img($id) {
        $resolution = array(
            'maxresdefault',
            'sddefault',
            'hqdefault',
            'mqdefault',
            'default'
        );

        $url = 'https://img.youtube.com/vi/' . $id . '/sddefault.jpg';

        return $url;
    }
}

if (! function_exists('slug')) {
    function slug($str = null, $table = null, $id = null, $separator = '-', $lowercase = true, $l = 'pt', $t = null)
    {
        if(empty($str))
            return '';

        $CI = &get_instance();
        $CI->load->helper('text');
        $str = str_replace("&nbsp;", " ", $str);
        $str = str_replace("_", "-", $str);
        $str = preg_replace("/\s+/", " ", $str);
        $str = trim(strip_tags(html_entity_decode($str)));
        $str = convert_accented_characters($str);
        $str = url_title($str, $separator, $lowercase);
        if ($table) {
            $unique = false;
            $count = 1;
            $slug = $str;
            while (!$unique) {
                if(!$t){
                    $CI->db->select('COUNT(*) AS total')->from($table)->where('(slug = "'.$slug.'" OR slug LIKE "%/'.$slug.'")');
                }else{
                    $CI->db->select('COUNT(*) AS total')->from($table)->where('(slug_'.$l.' = "'.$slug.'" OR slug_'.$l.' LIKE "%/'.$slug.'")');
                }
                if ($id) {
                    if(is_array($id)) {
                        $CI->db->where($id);
                    } else {
                        $CI->db->where('id !=', $id);
                    }
                }
                $query = $CI->db->get();
                $query = $query->row();
                if ((int) $query->total == 0) {
                    $unique = true;
                    $str = $slug;
                } else {
                    $slug = $str.'-'.$count++;
                }
            }
        }

        return $str;
    }
}