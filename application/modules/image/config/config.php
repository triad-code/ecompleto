<?php

/**
* Extensões de imagens permitidas
*/
$config['image_extensions'] = array('jpg', 'png', 'jpeg', 'gif','JPG', 'PNG', 'JPEG', 'GIF');

/**
* Caminho da imagem de erro padrão
*/ 
$config['image_error'] = APPPATH.'modules/image/config/error.jpg';

/**
* nome da pasta de cache
*/ 
$config['image_cache_dir'] = 'cache/wideimage/'/*date("my")*/;


/**
* Nivel de compressão dos arquivos PNG
*/ 
$config['png_comp_level']  = 6;