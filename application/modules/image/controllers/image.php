<?php if ( ! defined('BASEPATH')){exit('No direct script access allowed');}

/**
* Redimensionamento de imagens baseado na biblioteca wideimage(http://wideimage.sourceforge.net/)
*
* @package ezoom_cms
* @subpackage image
* @category modules
* @author Flávio Zantut Nogueira <flavio@ezoom.com.br>
*
* Adaptado para no-sparks + diversas correções e melhorias
* @author Fábio A. Neis
*/
class Image extends MY_Controller {

    private $extensions;
    private $error_image;
    private $cache_dir;
    private $png_comp_level;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('file');
        $this->load->library('WideImage');

        $this->config->load('config');
        $this->extensions = $this->config->item('image_extensions');
        $this->error_image = $this->config->item('image_error');
        $this->cache_dir =  $this->config->item('image_cache_dir');
        $this->png_comp_level =  $this->config->item('png_comp_level');

        //Cria o diretório de cache caso não exista
        if(!is_dir("userfiles/{$this->cache_dir}"))
            $this->createDir("userfiles/{$this->cache_dir}");
    }

    /**
    * Redimensionamento
    * exemplo: http://server/CI/image/resize?src=temp/imagem.jpg&w=800&h=800&q=75&fit=outside
    *
    * @param string $src caminho da imagem
    * @param integer|% $w largura, default: 100
    * @param integer|% $h altura, default: 100
    * @param % $q qualidade, default: 75
    * @param 'inside'|'outside'|'fill'  $fit  ajuste,  default: inside
    */
    public function resize()
    {
        $src = $this->input->get('src') ?$this->input->get('src') : '';
        $w = $this->input->get('w') ?$this->input->get('w') : '100';
        $h = $this->input->get('h') ?$this->input->get('h') : '100';
        $q = $this->input->get('q') ?$this->input->get('q') : '75';
        $fit = $this->input->get('fit') ?$this->input->get('fit') : 'inside';
        $noCache = $this->input->get('nocache') ? TRUE : FALSE;

        $file = htmlentities(urldecode($src));

        if(!is_file($file) && !preg_match("/^http/i", $src))
        {
            log_message('error', "load image  $file");
            $ext = pathinfo($this->error_image, PATHINFO_EXTENSION);
            $this->wideimage
                ->load($this->error_image)
                ->resize($w, $h, $fit)
                ->output($ext, $q);
        }

        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $file_name = md5("{$src}{$w}{$h}{$fit}{$q}") . '.' . $ext;

        //allowed extensions
        if (!preg_match("/^" . implode("|", $this->extensions) . "$/", $ext))
        {
            show_404('image/resize/'.$file);
        }

        if( !is_file("userfiles/{$this->cache_dir}/{$file_name}") || $noCache) {
            $q = $ext=='png'?$this->png_comp_level: $q;

            $this->wideimage
                ->load($file)
                ->resize($w, $h, $fit)
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        } else  if( filemtime("userfiles/{$this->cache_dir}/{$file_name}") < filemtime($file) ) {
            $q = $ext=='png'?$this->png_comp_level: $q;

            $this->wideimage
                ->load($file)
                ->resize($w, $h, $fit)
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        } else {
            log_message('info', "load cache image $file_name");
        }

        if ($noCache){

            $this->wideimage
                ->load($file)
                ->resize($w, $h, $fit)
                ->output($ext, $q);

        } else
            $this->output($file_name);
    }

    public function resize_canvas()
    {
        $src = $this->input->get('src') ?$this->input->get('src') : '';
        $w = $this->input->get('w') ?$this->input->get('w') : '100';
        $h = $this->input->get('h') ?$this->input->get('h') : '100';
        $fit = $this->input->get('fit') ?$this->input->get('fit') : 'inside';
        $q = $this->input->get('q') ?$this->input->get('q') : '75';
        $cw = $this->input->get('cw') ?$this->input->get('cw') : $w;
        $ch = $this->input->get('ch') ?$this->input->get('ch') : $h;
        $t = $this->input->get('t') ?$this->input->get('t') : 'center';
        $l = $this->input->get('l') ?$this->input->get('l') : 'center';
        $noCache = $this->input->get('nocache') ? TRUE : FALSE;

        $file = htmlentities(urldecode($src));

        if(!is_file($file) && !preg_match("/^http/i", $src))
        {
            log_message('error', "load image  $file");
            $ext = pathinfo($this->error_image, PATHINFO_EXTENSION);
            $this->wideimage
                ->load($this->error_image)
                ->resize($w, $h, $fit)
                ->crop($l, $t, $cw, $ch)
                ->output($ext, $q);
        }

        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $file_name = md5("canvas{$src}{$w}{$h}{$fit}{$cw}{$ch}{$l}{$t}{$q}") . '.' . $ext;

        //allowed extensions
        if (!preg_match("/^" . implode("|", $this->extensions) . "$/", $ext))
        {
            show_404('image/crop/'.$file);
        }

        if(!is_file("userfiles/{$this->cache_dir}/{$file_name}"))
        {
            $q = $ext=='png'?$this->png_comp_level: $q;
            //$white = $this->wideimage->allocateColor(255, 255, 255);
            $this->wideimage
                ->load($file)
                ->resize($w, $h, $fit)
                //->crop($l, $t, $cw, $ch)
                ->resizeCanvas($w, $h,  'center', 'center',  $this->wideimage->load($file)->allocateColor(255, 255, 255))
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        } else if( filemtime("userfiles/{$this->cache_dir}/{$file_name}") < filemtime($file) ) {
            $q = $ext=='png'?$this->png_comp_level: $q;
            //$white = $this->wideimage->allocateColor(255, 255, 255);
            $this->wideimage
                ->load($file)
                ->resize($w, $h, $fit)
                //->crop($l, $t, $cw, $ch)
                ->resizeCanvas($w, $h,  'center', 'center',  $this->wideimage->load($file)->allocateColor(255, 255, 255))
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        }else {
            log_message('info', "load cache image $file_name");
        }

        if ($noCache){

            $this->wideimage
                ->load($file)
                ->resize($w, $h, $fit)
                ->resizeCanvas($w, $h,  'center', 'center',  $this->wideimage->load($file)->allocateColor(255, 255, 255))
                ->output($ext, $q);

        } else
            $this->output($file_name);
    }

    /**
    * Mesclar
    * exemplo: http://192.168.1.190/revista_requinte/image/merge?src=temp/thumb_eventos.jpg&wtm=temp/banner.jpg
    *
    * @param string $src caminho da imagem
    * @param string $wtm caminho para a imagem que irá sobrepor
    * @param integer|% $w largura, default: 100
    * @param integer|% $h altura, default: 100
    * @param integer|% $top ditância de cima para baixo
    * @param integer|% $left ditância da esquerda para direita
    * @param integer $pct transparência da $wtm
    * @param integer $q qualidade da imagem
    */
    public function merge()
    {
                $src  = $this->input->get('src') ?$this->input->get('src') : '';
        $wtm  = $this->input->get('wtm') ?$this->input->get('wtm') : '';
        $w    = $this->input->get('w') ?$this->input->get('w') : '100';
        $h    = $this->input->get('h') ?$this->input->get('h') : '100';
        $top  = $this->input->get('top') ?$this->input->get('top') : 'center';
        $left = $this->input->get('left') ?$this->input->get('left') : 'center';
        $pct  = $this->input->get('pct') ?$this->input->get('pct') : '100';
        $q    = $this->input->get('q') ?$this->input->get('q') : '75';
        $noCache = $this->input->get('nocache') ? TRUE : FALSE;

        $file = htmlentities(urldecode($src));
        $file_wtm = htmlentities(urldecode($wtm));

        if(!is_file($file) || !is_file($file_wtm))
        {
            log_message('error', "load image  $file");
            $ext = pathinfo($this->error_image, PATHINFO_EXTENSION);
            $this->wideimage
                ->load($this->error_image)
                ->resize($w, $h)
                ->output($ext, $q);
        }

        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $file_name = md5("{$src}{$wtm}{$w}{$h}{$top}{$left}{$pct}{$q}") . '.' . $ext;

        //allowed extensions
        if (!preg_match("/^" . implode("|", $this->extensions) . "$/", $ext))
        {
            show_404('image/resize/'.$file);
        }

        if(!is_file("userfiles/{$this->cache_dir}/{$file_name}"))
        {
            $q = $ext=='png'?$this->png_comp_level: $q;
                     $watermark = $this->wideimage->load($file_wtm)->resize($w, $h);
            $this->wideimage
                ->load($file)
                ->resize($w, $h, 'outside') //'inside'|'outside'|'fill'
                ->crop('center', 'center', $w, $h)
                ->merge($watermark, $left, $top, $pct)
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        } else if( filemtime("userfiles/{$this->cache_dir}/{$file_name}") < filemtime($file) ) {
            $q = $ext=='png'?$this->png_comp_level: $q;
                     $watermark = $this->wideimage->load($file_wtm)->resize($w, $h);
            $this->wideimage
                ->load($file)
                ->resize($w, $h, 'outside') //'inside'|'outside'|'fill'
                ->crop('center', 'center', $w, $h)
                ->merge($watermark, $left, $top, $pct)
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        } else {
            log_message('info', "load cache image $file_name");
        }

        if ($noCache){

            $this->wideimage
                ->load($file)
                ->resize($w, $h, 'outside') //'inside'|'outside'|'fill'
                ->crop('center', 'center', $w, $h)
                ->merge($watermark, $left, $top, $pct)
                ->output($ext, $q);

        } else
            $this->output($file_name);
    }

    /**
    * Mesclar
    * exemplo: http://localhost/moneo/image/apply_mask?src=application/modules/home/assets/css/img/bg.jpg&mask=temp/mask.png&w=150
    *
    * @param string $src caminho da imagem
    * @param string $mask caminho para a imagem que irá sobrepor
    * @param integer|% $w largura, default: 100
    * @param integer|% $h altura, default: 100
    * @param integer|% $top ditância de cima para baixo
    * @param integer|% $left ditância da esquerda para direita
    * @param integer $pct transparência da $wtm
    * @param integer $q qualidade da imagem
    */
    public function apply_mask()
    {
        $src  = $this->input->get('src') ?$this->input->get('src') : '';
        $mask  = $this->input->get('mask') ?$this->input->get('mask') : '';
        $w    = $this->input->get('w') ?$this->input->get('w') : '100';
        $h    = $this->input->get('h') ?$this->input->get('h') : '100';
        $top  = $this->input->get('top') ?$this->input->get('top') : 'center';
        $left = $this->input->get('left') ?$this->input->get('left') : 'center';
        $pct  = $this->input->get('pct') ?$this->input->get('pct') : '100';
        $q    = $this->input->get('q') ?$this->input->get('q') : '75';
        $noCache = $this->input->get('nocache') ? TRUE : FALSE;

        $file = htmlentities(urldecode($src));
        $file_mask = htmlentities(urldecode($mask));

        if(!is_file($file) || !is_file($file_mask))
        {
            log_message('error', "load image  $file");
            $ext = pathinfo($this->error_image, PATHINFO_EXTENSION);
            $this->wideimage
                ->load($this->error_image)
                ->resize($w, $h)
                ->output($ext, $q);
        }

        $ext = 'png' /*pathinfo($file, PATHINFO_EXTENSION)*/;
        $file_name = md5("{$src}{$mask}{$w}{$h}{$top}{$left}{$pct}{$q}") . '.' . $ext;

        //allowed extensions
        if (!preg_match("/^" . implode("|", $this->extensions) . "$/", $ext))
        {
            show_404('image/resize/'.$file);
        }

        if(!is_file("userfiles/{$this->cache_dir}/{$file_name}"))
        {
            $q = $ext=='png'?$this->png_comp_level: $q;
            $watermark = $this->wideimage->load($file_mask)->resize($w, $h);

            $this->wideimage
                ->load($file)
                ->resize($w, $h, 'outside') //'inside'|'outside'|'fill'
                ->crop('center', 'center', $w, $h)
                ->applyMask($watermark, $left, $top, $pct)
                //->output('png')
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");

        } else if( filemtime("userfiles/{$this->cache_dir}/{$file_name}") < filemtime($file) ) {

            $q = $ext=='png'?$this->png_comp_level: $q;
            $watermark = $this->wideimage->load($file_mask)->resize($w, $h);

            $this->wideimage
                ->load($file)
                ->resize($w, $h, 'outside') //'inside'|'outside'|'fill'
                ->crop('center', 'center', $w, $h)
                ->applyMask($watermark, $left, $top, $pct)
                //->output('png')
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");

        } else {
            log_message('info', "load cache image $file_name");
        }

        if ($noCache){

            $this->wideimage
                ->load($file)
                ->resize($w, $h, 'outside') //'inside'|'outside'|'fill'
                ->crop('center', 'center', $w, $h)
                ->applyMask($watermark, $left, $top, $pct)
                ->output($ext, $q);

        } else
            $this->output($file_name);
    }

    /**
    * cropa imagem
    * exemplo: http://server/CI/image/crop?src=temp/imagem.jpg&w=800&h=800&q=75&l=left&t=top
    *
    * @param string $src caminho da imagem
    * @param integer|% $w largura, default: 100
    * @param integer|% $h altura, default: 100
    * @param % $q qualidade, default: 75
    * @param integer|'center'|'right'|'left'|'bottom'|'top'|'middle' $l coordenadas esquerda,  default: center
    * @param integer|'center'|'right'|'left'|'bottom'|'top'|'middle' $t coordenadas topo,  default: center
    */
    public function crop()
    {
        $src = $this->input->get('src') ?$this->input->get('src') : '';
        $w = $this->input->get('w') ?$this->input->get('w') : '100';
        $h = $this->input->get('h') ?$this->input->get('h') : '100';
        $q = $this->input->get('q') ?$this->input->get('q') : '75';
        $t = $this->input->get('t') ?$this->input->get('t') : 'center';
        $l = $this->input->get('l') ?$this->input->get('l') : 'center';
        $noCache = $this->input->get('nocache') ? TRUE : FALSE;

        $file = htmlentities(urldecode($src));

        if(!is_file($file) && !preg_match("/^http/i", $src))
        {
            log_message('error', "load image  $file");
            $ext = pathinfo($this->error_image, PATHINFO_EXTENSION);
            $this->wideimage
                ->load($this->error_image)
                ->crop($l, $t, $w, $h)
                ->output($ext, $q);
        }

        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $file_name = md5("{$src}{$w}{$h}{$l}{$t}{$q}") . '.' . $ext;

        //allowed extensions
        if (!preg_match("/^" . implode("|", $this->extensions) . "$/", $ext))
        {
            show_404('image/crop/'.$file);
        }

        if(!is_file("userfiles/{$this->cache_dir}/{$file_name}"))
        {
            $q = $ext=='png'?$this->png_comp_level: $q;
            $this->wideimage
                ->load($file)
                ->crop($l, $t, $w, $h)
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        } else if( filemtime("userfiles/{$this->cache_dir}/{$file_name}") < filemtime($file) ) {
            $q = $ext=='png'?$this->png_comp_level: $q;
            $this->wideimage
                ->load($file)
                ->crop($l, $t, $w, $h)
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        } else {
            log_message('info', "load cache image $file_name");
        }

        if ($noCache){

            $this->wideimage
                ->load($file)
                ->crop($l, $t, $w, $h)
                ->output($ext, $q);

        } else
            $this->output($file_name);
    }

    /**
    * redimensiona e cropa uma imagem
    * exemplo: http://server/CI/image/resize_crop?src=temp/imagem.jpg&w=800&h=800&q=75&l=center&t=center&cw=500&ch=500
    *
    * @param string $src caminho da imagem
    * @param integer|% $w largura, default: 100
    * @param integer|% $h altura, default: 100
    * @param 'inside'|'outside'|'fill'  $fit  ajuste,  default: inside
    * @param % $q qualidade, default: 75
    * @param integer|% $cw largura do crop, default: $w
    * @param integer|% $ch altura do crop, default: $c
    * @param integer|'center'|'right'|'left'|'bottom'|'top'|'middle' $l coordenadas esquerda,  default: center
    * @param integer|'center'|'right'|'left'|'bottom'|'top'|'middle' $t coordenadas topo,  default: center
    */
    public function resize_crop()
    {
        $src = $this->input->get('src') ?$this->input->get('src') : '';
        $w = $this->input->get('w') ?$this->input->get('w') : '100';
        $h = $this->input->get('h') ?$this->input->get('h') : '100';
        $fit = $this->input->get('fit') ?$this->input->get('fit') : 'outside';
        $q = $this->input->get('q') ?$this->input->get('q') : '75';
        $cw = $this->input->get('cw') ?$this->input->get('cw') : $w;
        $ch = $this->input->get('ch') ?$this->input->get('ch') : $h;
        $t = $this->input->get('t') ?$this->input->get('t') : 'center';
        $l = $this->input->get('l') ?$this->input->get('l') : 'center';
        $noCache = $this->input->get('nocache') ? TRUE : FALSE;

        $file = htmlentities(urldecode($src));

        if(!is_file($file) && !preg_match("/^http/i", $src))
        {
            log_message('error', "load image  $file");
            $ext = pathinfo($this->error_image, PATHINFO_EXTENSION);
            $this->wideimage
                ->load($this->error_image)
                ->resize($w, $h, $fit)
                ->crop($l, $t, $cw, $ch)
                ->output($ext, $q);
        }

        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $file_name = md5("{$src}{$w}{$h}{$fit}{$cw}{$ch}{$l}{$t}{$q}") . '.' . $ext;

        //allowed extensions
        if (!preg_match("/^" . implode("|", $this->extensions) . "$/", $ext))
        {
            show_404('image/crop/'.$file);
        }

        if(!is_file("userfiles/{$this->cache_dir}/{$file_name}"))
        {
            $q = $ext=='png'?$this->png_comp_level: $q;
            $this->wideimage
                ->load($file)
                ->resize($w, $h, $fit)
                ->crop($l, $t, $cw, $ch)
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        } else if( filemtime("userfiles/{$this->cache_dir}/{$file_name}") < filemtime($file) ) {
            $q = $ext=='png'?$this->png_comp_level: $q;
            $this->wideimage
                ->load($file)
                ->resize($w, $h, $fit)
                ->crop($l, $t, $cw, $ch)
                ->saveToFile("userfiles/{$this->cache_dir}/{$file_name}", $q);
            log_message('info', "save image $file_name");
        } else {
            log_message('info', "load cache image $file_name");
        }

        if ($noCache){

            $this->wideimage
                ->load($file)
                ->resize($w, $h, $fit)
                ->crop($l, $t, $cw, $ch)
                ->output($ext, $q);

        } else
            $this->output($file_name);
    }

    /**
    * Remove todas as imagens do cache
    */
    public function clear()
    {
        $scanned_directory = array_diff(scandir("userfiles/{$this->cache_dir}"), array('..', '.'));
        foreach ($scanned_directory as $dir) {
            $this->rrmdir("userfiles/{$this->cache_dir}{$dir}");
        }
    }

    /**
    * Exibe a imagem
    *
    */
    private function output($file_name)
    {
        $ext = pathinfo("userfiles/{$this->cache_dir}{$file_name}", PATHINFO_EXTENSION);
        header('Content-Type: image/'.$ext);
        header('Content-Disposition: inline; filename="'.$file_name.'";');
        readfile("userfiles/{$this->cache_dir}{$file_name}");

    }
    /**
    * Remove um diretorio e os arquivos contodos nele
    *
    * @param string $dir caminho do diretorio
    */
    private function rrmdir($dir) {
        if (is_dir($dir))
        {
            $objects = scandir($dir);
            foreach ($objects as $object)
            {
                if ($object != "." && $object != "..")
                {
                    if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    /**
     * Cria o diretório
     * @param  [string] $sourceDirPath caminho a ser criado
     * @return boolean
     */
    private function createDir($sourceDirPath) {
        $res = false;
        if(mkdir($sourceDirPath, 0777, true)) {
            $this->addGitIgnore($sourceDirPath);
            $res = true;
        }

        return $res;
    }

    /**
     * Adiciona o arquivo .gitignore
     */
    public function addGitIgnore($sourceDirPath) {
        //Adiciona o gitignore
        return file_put_contents($sourceDirPath.'/.gitignore', utf8_encode("*\r\n!.gitignore"));
    }
}

// End of file image.php
// Location: ./modules\image\controllers\image.php