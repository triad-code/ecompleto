/*
 * @subpackage main
 * @category js
 * @author Michael Cruz
 * @copyright 2022
 */

"use strict";
var app;
var $window = $(window);

function Main() {
    this.init();
};

Main.prototype.init = function () {
    var self = this;
};

$(document).ready(function () {
    app = new Main();
});