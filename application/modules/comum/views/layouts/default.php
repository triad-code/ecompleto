<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title><?php echo $pageTitle; ?></title>

    <meta name="description" content="Desenvolvimento de Lojas Virtuais, E-commerce e Integrações com Marketplaces">
    <link rel="icon" type="image/ico" sizes="32x32" href="<?php echo $_siteUrls["baseImg"]; ?>icon/favicon.ico">
    <meta name="msapplication-TileColor" content="#0083b6">
    <meta name="theme-color" content="#0083b6">
    <meta name="description" content="">
    <meta name="author" content="Michael Cruz">
    <meta name="MobileOptimized" content="320">
    <meta name="HandheldFriendly" content="True">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="cleartype" content="on">

    <link rel="stylesheet" type="text/css" media="all" href="<?php echo $_siteUrls["baseCss"] . "main.css?v=" . $this->config->item('version'); ?>" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo $_siteUrls["baseCss"] . "font-awesome.min.css"; ?>" />
    <?php echo $template['metadataCss']; ?>
    <?php echo $template['metadata']; ?>
    <script src="<?php echo $_siteUrls["baseJs"] . "jquery.min.js"; ?>"></script>
    <script src="<?php echo $_siteUrls["basePlugins"] . "jquery.placeholder.js"; ?>"></script>
    <script src="<?php echo $_siteUrls["baseJs"] . "main.js?v=". $this->config->item('version'); ?>"></script>
    <script src="<?php echo $_siteUrls["basePlugins"] . "modernizr.js"; ?>"></script>
    <script src="<?php echo $_siteUrls["basePlugins"] . "jquery.validate.min.js"; ?>"></script>
    <script src="<?php echo $_siteUrls["basePlugins"] . "jquery.cookie.js"; ?>"></script>
    <link type="text/css" media="all" href="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&display=swap" rel="stylesheet">
    <script type="text/javascript">
        var
            baseUrl = '<?php echo $this->config->base_url(); ?>',
            siteUrl = '<?php echo $this->config->site_url(); ?>',
            baseImg = '<?php echo $_siteUrls["baseImg"]; ?>',
            baseCss = '<?php echo $_siteUrls["baseCss"]; ?>',
            baseJs = '<?php echo $_siteUrls["baseJs"]; ?>';
        basePlugins = '<?php echo $_siteUrls["basePlugins"]; ?>';
        cmsPrefix = '<?php echo $_siteUrls["cmsPrefix"]; ?>';
        moduleImg = '<?php echo $_siteUrls["moduleImg"]; ?>';
        modulePhotos = '<?php echo $_siteUrls["modulePhotos"]; ?>';
    </script>

    <?php echo $this->_loadedAssets; ?>
    <?php echo $template['metadataJs']; ?>
</head>

<body>

    <section id="wrapper">
        <?php echo $template['partials']['header']; ?>

        <?php echo $template['body']; ?>

        <?php echo $template['partials']['footer']; ?>

    </section>

</body>

</html>