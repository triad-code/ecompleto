<section id="header">
    <div class="common-limiter">
        <a id="logo" alt="Desenvolvimento de Lojas Virtuais, E-commerce e Integrações com Marketplaces" title="Desenvolvimento de Lojas Virtuais, E-commerce e Integrações com Marketplaces" href="https://e-completo.com.br/" target="_blank">
            <img class="logo" src="<?php echo $_siteUrls["baseImg"] . 'logo.png'; ?>" alt="Desenvolvimento de Lojas Virtuais, E-commerce e Integrações com Marketplaces">
        </a>
    </div>
</section>