<?php if ( ! defined('BASEPATH')){exit('No direct script access allowed'); }

class Home_m extends MY_model {

    public function __construct (){
        parent::__construct();
    }

    public function get_order($id = null)
    {
        $this->db->select("pedidos.*,
                           clientes.nome,
                           clientes.email,
                           clientes.cpf_cnpj,
                           clientes.data_nasc,
                           pedido_situacao.descricao,
                           lojas_gateway.id_gateway,
                           pedidos_pagamentos.*")
                 ->select("(SELECT endpoint FROM gateways WHERE gateways.id = lojas_gateway.id_gateway LIMIT 1) AS endpoint")
                 ->from("pedidos")
                 ->join("clientes", "clientes.id = pedidos.id_cliente", "INNER")
                 ->join("pedido_situacao", "pedido_situacao.id = pedidos.id_situacao", "INNER")
                 ->join("lojas_gateway", "lojas_gateway.id_loja = pedidos.id_loja", "INNER")
                 ->join("pedidos_pagamentos", "pedidos_pagamentos.id_pedido = pedidos.id", "INNER")
                 ->order_by("pedidos.id", "DESC");

        if($id){
            $this->db->where("pedidos.id", $id);
        }

        $query = $this->db->get();
        return ($id ? $query->row_array() : $query->result());
    }

    public function update_order($data)
    {
        $this->db->trans_start();
        
        $this->db->where('id_pedido', $data['id_order'])
                 ->update('pedidos_pagamentos', array('retorno_intermediador' => $data['retorno_intermediador'], 'data_processamento' => $data['data_processamento']));

        $this->db->where('id', $data['id_order'])
                 ->update('pedidos', array('id_situacao' => $data['status']));

        $this->db->trans_complete();
        return $this->db->trans_status();
    }
}