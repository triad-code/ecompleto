<section id="list">
    <div class="common-limiter">
        <h1>Pedidos</h1>
        <div class="middle">
            <?php
            if ($list) {
                foreach ($list as $each_list) { ?>
                    <div class="row">
                        <div class="col"><?php echo $each_list->nome; ?></div>
                        <div class="col"><?php echo 'Nº pedido #' . $each_list->id; ?></div>
                        <div class="col"><?php echo $each_list->descricao; ?></div>
                        <div class="col"><?php echo 'R$' . number_format($each_list->valor_total, 2, ',', '.'); ?></div>
                        <div class="col">
                            <?php if ($each_list->id_gateway == 1 && $each_list->id_formapagto == 3) { ?>
                                <button data-id="<?php echo $each_list->id_pedido; ?>" <?php echo ($each_list->id_situacao != 1 ? "disabled='true'" : ""); ?> class="btn-action"><?php echo ($each_list->id_situacao != 1 ? $each_list->descricao : "Processar Pagamento"); ?> </button>
                            <?php } else { ?>
                                <button disabled='true' class="btn-action"><?php echo $each_list->descricao; ?></button>
                            <?php } ?>
                        </div>
                    </div>
                <?php }
            } else { ?>
                <h2>Nenhum pedido cadastrado</h2>
            <?php } ?>
        </div>
    </div>
</section>