<?php if ( ! defined('BASEPATH')){exit('No direct script access allowed'); }
class Home extends MY_controller {

    public function __construct (){
        parent::__construct();
        $this->load->model('home_m');
        $this->autoLoadAssets();
    }

    public function index ()
    {
        /** BUSCA OS PEDIDOS */
        $list = $this->home_m->get_order();
        $this->template->set('pageTitle', 'Desenvolvimento de Lojas Virtuais, E-commerce e Integrações com Marketplaces')
                       ->set('list', $list)
                       ->build('home');
    }

    public function process ()
    {
        /** CASO NÃO SEJA INFORMADO O ID APRESENTA PÁGINA 404 */
        if (!$this->input->is_ajax_request())
            show_404();

        /** BUSCA O PEDIDO PELO ID INFORMADO */
        $id = $this->input->post('id');
        $order = (Object) $this->home_m->get_order($id);

        /** INÍCIO ABERTURA CURL PARA REQUISIÇÃO COM A API */
        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, $order->endpoint . 'exams/processTransaction');
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cr, CURLOPT_POST, TRUE);

        /** API KEY FORNECIDA JUNTO COM A DOCUMENTAÇÃO */
        $key = '639c3c33fe3f324750356306679fbdf6';
        $headers = array(
            'Authorization:' . $key
        );
        curl_setopt($cr, CURLOPT_HTTPHEADER, $headers);

        /** AJUSTA O FORMATO DO VENCIMENTO */
        $vencimento = explode('-', $order->vencimento);

        /** MONTA O ARRAY A SER ENVIADO PELO CURL */
        $send = array(
            "external_order_id" => (Int) $order->id_pedido,
            "amount" => (Double) $order->valor_total,
            "card_number" => $order->num_cartao,
            "card_cvv" => $order->codigo_verificacao,
            "card_expiration_date" => trim($vencimento['1'] . substr($vencimento['0'],2)),
            "card_holder_name" => $order->nome_portador,
            "customer" => array(
                "external_id" => $order->id_cliente,
                "name" => $order->nome,
                "type" => "individual",
                "email" => $order->email,
                "documents" => array(
                    "type" => "cpf",
                    "number" => $order->cpf_cnpj
                ),
                "birthday" => $order->data_nasc
            )
        );
        curl_setopt($cr, CURLOPT_POSTFIELDS, json_encode($send));
        /** EXECUTA A REQUISIÇÃO */
        $retorno = curl_exec($cr);
        /** FECHA O CURL */
        curl_close($cr);
        if(isset($retorno)){
            $return = json_decode($retorno);

            switch ($return->Transaction_code) {
                case '01':
                    $class = "warning";
                    break;
                case '02':
                    $class = "warning";
                    break;
                case '03':
                    $class = "error";
                    break;
                case '04':
                    $class = "error";
                    break;
                default:
                    $class = "success";
                    break;
            }
            /** AJUSTA CLASSE DE APRESENTAÇÃO NA VIEW */
            $return->Class = $class;

            /** MONTA O ARRAY PRA ATUALIZAÇÃO DO PEDIDO */
            $update = array(
                'id_order' => $order->id_pedido,
                'retorno_intermediador' => $return->Message,
                'data_processamento' => date('d-m-Y H:i:s'),
                'status' => ($return->Transaction_code != 00 ? 3 : 2)
            );
            /** FUNÇÃO DE ATUALIZAÇÃO */
            $this->home_m->update_order($update);


        }else{
            $return = array(
                'Class' => 'error',
                'Message' => "Erro de comunicação com o servidor"
            );
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($return));
    }
}