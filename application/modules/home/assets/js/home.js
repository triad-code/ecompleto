/**
 * js
 *
 * @subpackage home
 * @category js
 * @author Michael Cruz
 * @copyright 2022
 */

"use strict";
function Home() {
    this.init();
};

Home.prototype.init = function () {
    var self = this;
    self.process();
};

Home.prototype.process = function () {
    var self = this;
    $('.btn-action').on('click', function(e){
        if ($(this).hasClass('loading'))
            return false;
        $(this).addClass('loading').attr('disabled', true);
        var id = $(this).data(id);

        $.ajax({
            url: siteUrl + 'home/process',
            type: 'POST',
            data: id,
            success: function (response) {
                Swal.fire({
                    title: '',
                    text: response.Message,
                    icon: response.Class,
                    showConfirmButton: false,
                    timer: 1500,
                });
                setTimeout(() => {
                    document.location.reload(true);
                }, 1600);
            }
        });
    });
}

$(document).ready(function () {
    new Home();
});