<?php
    class MY_Controller extends CI_Controller {

        protected $_module = '';
        protected $_moduleImg = '';
        protected $_baseUrl = '';
        protected $_siteUrl = '';
        protected $_baseImg = '';
        protected $_baseCss = '';
        protected $_baseJs = '';
        protected $_basePlugins = '';
        protected $_isCms = false;
        public $_cmsPrefix = 'cms';
        public $_loadedAssets = '';
        public $languages;
        public $current_lang;
        public $slug_model;
        public $model;
        public $models;

        public function __construct (){

            parent::__construct();
            $this->load->library('session');
            $this->load->library('template');
            $this->load->helper('url');
            $this->load->helper('string');
            $this->load->helper('text');
            $this->load->helper('language');
            $this->lang->load('comum/default');

            if(ENVIRONMENT == 'development'){
                $this->output->enable_profiler(FALSE);
            }

            $this->_module = $this->router->fetch_module();
            $this->_baseUrl = $this->config->item('base_url');
            $this->_siteUrl = $this->config->site_url();
            $this->languages = array_keys($this->config->config["supported_lang"]);
            $this->current_lang = $this->config->config['language_abbr'];
            $layoutPath = "comum";

            $this->_moduleImg = $this->_baseUrl . APPPATH . 'modules/'.$this->_module.'/assets/img/';
            $this->_baseImg = $this->_baseUrl . APPPATH . 'modules/'.$layoutPath.'/assets/img/';
            $this->_baseCss = $this->_baseUrl . APPPATH . 'modules/'.$layoutPath.'/assets/css/';
            $this->_baseJs = $this->_baseUrl . APPPATH . 'modules/'.$layoutPath.'/assets/js/';
            $this->_baseSvg = $this->_baseUrl . APPPATH . 'modules/'.$layoutPath.'/assets/svg/';
            $this->_basePlugins = $this->_baseUrl . APPPATH . 'modules/'.$layoutPath.'/assets/plugins/';
            $this->_modulePhotos = 'cms_fotos';

            $site_urls = array(
                'siteUrl'=> $this->_siteUrl,
                'baseJs'=> $this->_baseJs,
                'baseSvg'=> $this->_baseSvg,
                'basePlugins'=> $this->_basePlugins,
                'baseImg'=> $this->_baseImg,
                'baseCss' => $this->_baseCss,
                'cmsPrefix' => $this->_cmsPrefix,
                'module' => $this->_module,
                'moduleImg' => $this->_moduleImg,
                'modulePhotos' => 'cms_fotos'
            );

            $layout = $layoutPath.'/views/layouts/default';
            $partialHeader = $layoutPath.'/parts/header';
            $partialFooter = $layoutPath.'/parts/footer';


            $this->template->set("_siteUrls", $site_urls)
                            ->set_layout($layout)
                            ->set('current_lang', $this->current_lang)
                            ->set_partial('header', $partialHeader)
                            ->set_partial('footer', $partialFooter);
        }

        protected function autoLoadAssets (){

            if (!$this->input->post('loadAssets') && $this->_isCms)
                return;
            $module = "application/modules/".$this->_module."/assets/";
            $it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($module));

            while($it->valid()) {

                if (!$it->isDot()) {

                    $extensionTest = explode('.', $it->getSubPathName());
                    $extensionTest = strtolower($extensionTest[count($extensionTest)-1]);
                    if ($extensionTest == 'css' || $extensionTest == 'js'){

                        $file = str_replace("\\", "/", $this->_siteUrl.$module.$it->getSubPathName());

                        if ($it->getSubPath() == 'js'){
                            $file = '<script type="text/javascript" src="'.$file.'"></script>';
                            if ($this->_isCms)
                                echo $file;
                            else
                                $this->_loadedAssets .= $file;
                        }
                        else if ($it->getSubPath() == 'css'){
                            $file = '<link rel="stylesheet" type="text/css" media="all" href="'.$file.'" />';
                            if ($this->_isCms)
                                echo $file;
                            else
                                $this->_loadedAssets .= $file;
                        }

                    }
                }

                $it->next();
            }
        }

        public function permissionVerify ($module){

            $this->load->model('cms/cms_m');

            $moduleName = str_replace('cms_', '', $module);

            $data = $this->cms_m->getModules($moduleName);

            if(!in_array($data[0]->idModule, $this->session->userdata('permissions'))){
                header('HTTP/1.0 999 CMS without permission');
                die();
            }

        }

    }
?>